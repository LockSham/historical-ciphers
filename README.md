# Historical Ciphers
An implementation of six historical ciphers (Caesar, Hill, Linear, Simple Substitution, Transposition and Vigenere) in C. Historical ciphers is a simple program that allows you to encrypt or decrypt an ASCII file using any of the six listed ciphers. This project was developed as a leaning experience in programming a somewhat non trivial program in C. Non trivial in the sense of implementing everything from scratch from the matrix operations and data structures to the actual ciphers. Some concessions in the implementations of some of the algorithms/mathematical methods were made as they were deemed unnecessary for such a simple use case. For instance matrix multiplication is not vectorised.

## Getting Started
Historical ciphers uses the gnu18 C standard and gnu make with no extra external dependencies. 

### Building
Building this project is a simple case of cloning this repository and running `make` in the `src` directory.
```
git clone https://gitlab.com/LockSham/historical-ciphers historical-ciphers
cd historical-ciphers/src
make 
```

### Usage
Once built the program can be executed by executing the compiled program `hciphers` in the root directory of the project with arguments that follow the usage string `hciphers -e|-d [-t] [-o DEST] KEY INFILE`. 

Where:
 - `-e or -d` specifies that the program is either operating in encryption or decryption mode.
 - `-t` optionally specifies the program should run "*tests*" (not really tests more so sanity checking of the code).
 - `-o DEST` optionally specifies the file the program will output the encrypted/decrypted text into (defaults to INFILE).
 - `KEY` specifies the key file to use samples key files are stored in the sample-keys directory.
 - `INFILE` specifies the ASCII file to encrypt or decrypt.
