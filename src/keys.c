/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "utils.h"
#include "ciphers.h"
#include "keys.h"

// Cipher key processing functions
struct Cipher *process_transposition(struct key_data *data);
struct Cipher *process_caesar(struct key_data *data);
struct Cipher *process_substitution(struct key_data *data);
struct Cipher *process_vigenere(struct key_data *data);
struct Cipher *process_linear(struct key_data *data);
struct Cipher *process_hill(struct key_data *data);

// Utility functions
struct key_data *read_key_attributes(FILE *key_fp, int n);
int *comma_string_to_array(char *input, int size);

/*
 * Defines a data structure to hold a ciphers key data after being read from
 * a key file
 */
struct key_data
{
	int size;
	hash_table *h;
};

/*
 * Free and nullify the allocated memory used in
 * the key_data structure.
 */
void free_key_data(struct key_data **options)
{
	for (int i = 0; i<(*options)->h->size; i++) {
		free(((*options)->h->values[i]));
		(*options)->h->values[i] = NULL;
	}
	hash_delete((*options)->h);
	free(*options);
	*options = NULL;
}

/*
 * Read a ciphers key attributes from the file pointer 'key_fp'
 * with the attributes specified in the array of strings 'attributes'
 * of size 'n'.
 *
 * returns: a dynamically allocated array of strings with
 * the attributes in the order that they are defined in 'attributes'.
 * If not all the specified attributes are found, frees the allocated
 * memory and returns NULL.
 */
struct key_data *read_key_attributes(FILE *key_fp, int n)
{
	char line[256];
	char attr_str[150];
	char attribute[150];
	int count = 0;
	struct key_data *options = calloc(1, sizeof(struct key_data));
	if (!options) {
		fprintf(stderr,
			"Error: Could not allocate memory for key_data\n");
		return NULL;
	}
	options->size = n;

	options->h = hash_create(n*3);
	if (!options->h) {
		fprintf(stderr, "Error: Could not allocate memory for key hash table\n");
		free(options);
		return NULL;
	}
	// read the key attributes and store them in a hash table
	while (fgets(line, sizeof(line), key_fp)) {
		if (*line == '#' || *line == '\n' ) {
			continue;
		} else if (sscanf(line, "%150s %150[^\n]", attr_str, attribute) == 2) {
			strip_new_line(attribute);
			int in = hash_insert(options->h, attr_str,
					(void*)strdup(attribute));
			if (in == -1) {
				fprintf(stderr, "ERROR: Could not store key attribute\n");
				free_key_data(&options);
				return NULL;
			}
			count++;
		}
	}

	if (count > n) {
		fprintf(stderr, "Error: did not find all required, key attributes\n");
		free_key_data(&options);
		return NULL;
	}

	return options;
}

/*
 * Search the key file for the cipher type. The type declaration
 * is the first non-new line or comment line within the key file.
 */
char *read_cipher_type(FILE *key_fp)
{
	char *cipher = NULL;
	char line[256];
	fseek(key_fp, 0L, SEEK_SET);
	while (!cipher && fgets(line, sizeof(line), key_fp)) {
		if (*line == '#' || *line == '\n') {
			continue;
		}
		strip_new_line(line);
		cipher = strdup(line);
	}

	return cipher;
}

/*
 * Convert a comma separated string of integers into an array of
 * integers of size 'size'.
 *
 * Returns: An array of extracted integers of size 'size'
 * 	else returns NULL.
 */
int *comma_string_to_array(char *input, int size)
{
	char *cp = strtok(input, ",");
	if (!cp) {
		fprintf(stderr, "Error: No delimiter ',' in %s\n", input);
		return NULL;
	}

	int *array = malloc(size * sizeof(int));
	if (!array) {
		fprintf(stderr, "Error: Could not allocate memory for array\n");
		return NULL;
	}

	int count = 0;
	for (int i = 0; i<size; i++) {
		int number;
		if (cp && sscanf(cp, "%d", &number) == 1) {
			count++;
			array[i] = number;
			cp = strtok(NULL, ",");
		} else {
			fprintf(stderr, "Invalid token %s\n", cp);
			break;
		}
	}

	if (count != size) {
		fprintf(stderr,
			"Did not find %d tokens instead found %d\n", size, count);
		free(array);
		array = NULL;
	}
	return array;
}

/*
 * Get the integer (enum cipher_e) that corresponds to the specified cipher
 * name
 *
 * Returns:
 * 	If cipher_str is a valid cipher name: an integer representing the cipher.
 * Otherwise:
 * 	-1.
 */
int get_cipher_type(char *cipher_str)
{
	if (strcmp(cipher_str, "transposition") == 0) {
		return TRANSPOSITION;
	} else if (strcmp(cipher_str, "caesar") == 0) {
		return CAESAR;
	} else if (strcmp(cipher_str, "substitution") == 0){
		return SUBSTITUTION;
	} else if (strcmp(cipher_str, "vigenere") == 0) {
		return VIGENERE;
	} else if (strcmp(cipher_str, "linear") == 0) {
		return LINEAR;
	} else if (strcmp(cipher_str, "hill") == 0) {
		return HILL;
	}
	return -1;
}

/*
 * Process the Transposition cipher attributes received from a transposition key
 * file and create a cipher with the specified attributes from the file.
 *
 * Takes the key data struct that contains a hash table which should contain
 * valid transposition attribute data (for the attributes 'd' and 'f') and
 * creates an instance of the transposition cipher. Where 'd' is the period
 * of the block to permute and 'f' is an array of permuted character indices.
 *
 * Returns:
 * 	If attribute parsing and cipher creation succeeds A valid created
 * 	instance of the specified transposition cipher.
 * Otherwise:
 * 	NULL.
 */

struct Cipher *process_transposition(struct key_data *data)
{
	char *d_str = hash_lookup(data->h, (char*)"d");
	char *f_str = hash_lookup(data->h, (char*)"f");
	if ((intptr_t)d_str == -1 || (intptr_t)f_str == -1) {
		fprintf(stderr, "Error: Could not parse key data\n");
		return NULL;
	}

	int d = atoi(d_str);
	int *f = comma_string_to_array(f_str, d);
	if (!f) {
		fprintf(stderr, "Error: Could not create function f for"
				" transposition cipher\n");
		return NULL;
	}

	struct Cipher *cipher = cipher = create_transposition(d, f);
	free(f);
	if (!cipher) {
		fprintf(stderr, "Error: Could not create an instance of the"
				" transposition cipher\n");
		return NULL;
	}

	return cipher;
}

/*
 * Process the Caesar cipher data received from a Caesar cipher key file
 * and create a cipher with the specified attributes from the file.
 *
 * Takes the key_data attribute as input that should contain valid Caesar
 * cipher attribute data (for the attributes 'alphabet'(size) and the shift
 * amount 'j') and creates an instance of the specified Caesar cipher.
 *
 * Returns:
 * 	If attribute parsing and cipher creation are successful. An instance
 * 	of the specified cipher.
 * Otherwise:
 * 	NULL.
 */
struct Cipher *process_caesar(struct key_data *data)
{
	char *alphabet = hash_lookup(data->h, (char*)"alphabet");
	char *j_str = hash_lookup(data->h, (char*)"j");
	if ((intptr_t)alphabet == -1 || (intptr_t)j_str == -1) {
		fprintf(stderr, "Error: Could not parse key data\n");
		return NULL;
	}
	int j = atoi(j_str);

	struct Cipher *cipher = create_caesar(alphabet, j);
	if (!cipher) {
		fprintf(stderr, "Error: Could not create an instance of"
				" the caesar cipher\n");
		return NULL;
	}

	return cipher;
}

/*
 * Process the substitution cipher data received from a substitution cipher key
 * file and create the substitution cipher from the specified attributes.
 *
 * Takes the key data from 'data' which should contain a hash table with
 * the attributes for a substitution cipher and creates a cipher from these
 * attributes. The attributes that should be contained in the hash table is
 * the defined 'alphabet' and 'substitutions' strings.
 *
 * Returns:
 * 	If the attribute parsing and cipher creation are successful, an
 * 	instance of the specified cipher.
 * Otherwise.
 * 	NULL.
 */
struct Cipher *process_substitution(struct key_data *data)
{
	char *alphabet = hash_lookup(data->h, (char*)"alphabet");
	char *substitutions = hash_lookup(data->h, (char*)"substitutions");

	if ((intptr_t)alphabet == -1 || (intptr_t)substitutions == -1) {
		fprintf(stderr, "Error: Could not parse key data\n");
		return NULL;
	}

	struct Cipher *cipher = create_substitution(alphabet, substitutions);
	if (!cipher) {
		fprintf(stderr, "Error: Could not create an instance of"
				" the substitution cipher\n");
		return NULL;
	}

	return cipher;

}

/*
 * Process the Vigenere cipher attributes received from a Vigenere cipher key
 * file and create a Vigenere cipher with the specified attributes.
 *
 * Takes the attributes from 'data' which contains a hash table with attributes
 * for a Vigenere cipher and creates a cipher from these attributes. The
 * attributes that should be contained in the hash table is the defined
 * 'alphabet' and a 'key' string.
 *
 * Returns:
 * 	If the attribute parsing and cipher creation are successful, an
 * 	instance of the specified Vigenere cipher.
 * Otherwise:
 * 	NULL.
 */
struct Cipher *process_vigenere(struct key_data *data)
{
	char *alphabet = hash_lookup(data->h, (char*)"alphabet");
	char *key_str = hash_lookup(data->h, (char*)"key");
	if ((intptr_t)alphabet == -1 || (intptr_t)key_str == -1) {
		fprintf(stderr, "Error: Could not parse key data\n");
		return NULL;
	}

	struct Cipher *cipher = create_vigenere(alphabet, key_str);
	if (!cipher) {
		fprintf(stderr, "Error, Could not create an instance of"
				" the vigenere cipher\n");
		return NULL;
	}
	return cipher;
}

/*
 * Process the Linear cipher attributes received from a Linear cipher key
 * file and create a Linear cipher with the specified attributes.
 *
 * Takes the attributes from 'data' which contains a hash table with attributes
 * for a Linear cipher and creates a cipher from these attributes. The
 * attributes that should be contained in the hash table are the defined
 * 'alphabet' and the coefficients 'a' and 'b' of the Linear equation.
 *
 * Returns:
 * 	If the attribute parsing and cipher creation are successful, an
 * 	instance of the specified Linear cipher.
 * otherwise:
 * 	NULL.
 */
struct Cipher *process_linear(struct key_data *data)
{
	char *alphabet = hash_lookup(data->h, (char*)"alphabet");
	char *a_str = hash_lookup(data->h, (char*)"a");
	char *b_str = hash_lookup(data->h, (char*)"b");
	if ((intptr_t)alphabet == -1 || (intptr_t)a_str == -1
			|| (intptr_t)b_str == -1) {
		fprintf(stderr, "Error: Could not parse key data\n");
		return NULL;
	}

	int a = atoi(a_str);
	int b = atoi(b_str);

	struct Cipher *cipher = create_linear(a, b, alphabet);
	if (!cipher) {
		fprintf(stderr, "Error: Could not create an instance of the"
				" linear cipher\n");
		return NULL;
	}
	return cipher;
}

/*
 * Process the Hill cipher attributes received from a Hill cipher key file and
 * create a Hill cipher with the specified attributes.
 *
 * Takes the attributes from 'data' which contains a hash table with attributes
 * for a Hill cipher and create an instance of a Hill cipher with the specified
 * attributes. The attributes that should be contained in the hash table are
 * the dimensions of the key matrix 'd' (where the dimensions are 'd'x'd'), and
 * the elements of said matrix.
 *
 * Returns:
 * 	If the attributes parsing and cipher creation are successful, an
 * 	instance of the specified Hill cipher.
 */
struct Cipher *process_hill(struct key_data *data)
{
	char *d_str = hash_lookup(data->h, (char*)"d");
	char *keys_str = hash_lookup(data->h, (char*)"keys");
	char *alphabet = hash_lookup(data->h, (char*)"alphabet");
	if ((intptr_t)d_str == -1 || (intptr_t)keys_str == -1) {
		fprintf(stderr, "Error: Could not parse key data in"
				"processing cipher\n");
		return NULL;
	}

	int d = atoi(d_str);
	int *keys = comma_string_to_array(keys_str, d*d);
	if (!keys) {
		fprintf(stderr, "Error: Could not create function f for"
				" hill cipher\n");
		return NULL;
	}

	struct Cipher *cipher = create_hill(alphabet, d, keys);
	if (!cipher) {
		fprintf(stderr, "Error: Could not create an instance of the"
				" hill cipher\n");

	}

	free(keys);
	return cipher;
}

/*
 * Process the provided cipher key file for the specified cipher type.
 *
 * Takes the 'cipher_type' to initialise the array of string key attributes
 * for the specified cipher and extracts the attributes specified
 *
 * Returns:
 * 	extracts the  with the extracted data.
 * else:
 * 	returns NULL.
 */
struct key_data *process_key_file(FILE *key_fp, int *cipher_type)
{
	char *cipher_str = NULL;
	struct key_data *options = NULL;
	int size;

	cipher_str = read_cipher_type(key_fp);
	if (!cipher_str) {
		fprintf(stderr, "Error: Cipher type string not found\n");
		return NULL;
	}

	*cipher_type = get_cipher_type(cipher_str);
	free(cipher_str);

	switch (*cipher_type) {
		case TRANSPOSITION:
		case CAESAR:
		case SUBSTITUTION:
		case VIGENERE:
			size = 2;
			break;
		case LINEAR:
		case HILL:
			size = 3;
			break;
		default:
			fprintf(stderr, "Error: Did receive a valid cipher\n");
			return NULL;
	}

	options = read_key_attributes(key_fp, size);
	if (!options) {
		fprintf(stderr, "Error: Did not find all relevant"
				" cipher key options in file\n");
		return NULL;
	}

	return options;
}

/*
 * Create an instance of the cipher specified by the string cipher_str
 */
struct Cipher *create_cipher(struct key_data *options, int cipher_type)
{
	struct Cipher *cipher = NULL;
	switch (cipher_type) {
		case TRANSPOSITION:
			cipher = process_transposition(options);
			break;
		case CAESAR:
			cipher = process_caesar(options);
			break;
		case SUBSTITUTION:
			cipher = process_substitution(options);
			break;
		case VIGENERE:
			cipher = process_vigenere(options);
			break;
		case LINEAR:
			cipher = process_linear(options);
			break;
		case HILL:
			cipher = process_hill(options);
			break;
		default:
			fprintf(stderr, "Error: Not a valid cipher\n");
			break;
	}

	if (!cipher) {
		fprintf(stderr, "Error: Cipher not created\n");
	}

	return cipher;
}
