/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H_
#define UTILS_H_

/*
 * A modulus function that is defined for negative numbers
 * for both a and b. If a is less than 0, C's % operator
 * will return a negative number. To fix this add b to to
 * the remainder. If b < 0 return mod(a,-b), to make b positive.
 */
int mod(int a, int b);

/*
 * Calculate the modular multiplicative inverse of a mod b;
 */
int mod_inverse(int a, int b);

/*
 * Calculate the GCD(a,b) of a and b using the
 * Euclidean algorithm.
 */
int gcd(int a, int b);


/*
 * Strip the new line from the end of the string if it
 * exists
 */
void strip_new_line(char *str);
//-----------------------------------------------------------------------------

/*
 * Structure defining a hash_table, that
 * stores it's size as well as the key and value
 * pairs.
 */
typedef struct hash_table {
	int size;
	int probe;
	char **keys;
	void **values;
} hash_table;

/*
 * Create and return a new allocated hash_table
 * with the 'size' possible characters of the
 * alphabet. The size of the hash_table is stored
 * in the hash tables size variable.
 *
 * Returns: If the memory is allocated a new hash_table
 *          that has its variables allocated ready for use.
 *          Otherwise returns NULL.
 */
hash_table *hash_create(int size);

/*
 * Free the allocated memory that is in use by the
 * hash table
 */
void hash_delete(hash_table *h);

/*
 * return the hashed index for the array element that
 * corresponds to the key value.
 */
unsigned int hash_index(int size, char *key);

/*
 * Return the probe size for a hashtable probe
 * that relates to the corresponding hashed `key`
 */
unsigned int hash_probe(int size, char *key);

/*
 * Looks up the value associated with the key 'key'.
 *
 * Returns: If a value is associated with the key that value
 *          is returned, otherwise NULL is returned.
 */
void *hash_lookup(hash_table *h, char *key);

/*
 * Insert a new key and value pair into the hash table.
 */
int hash_insert(hash_table *h, char *key, void *value);

/*
 * Calculate the next prime number after `n`
 */
int next_prime(int n);

/*
 * Calculate the previous prime number before `n`
 */
int prev_prime(int n);

/*
 * Read the entire contents of a file into a dynamically allocated buffer.
 *
 * Returns a dynamically allocated char pointer buffer filled with the
 * contents of `file` on success, otherwise returns NULL.
 */
char *read_all(FILE *file);

/*
 * Prepare a message to be encrypted/decrypted.
 *
 * Prepares the message `test` for encryption/decryption by removing all
 * the new line characters from the text and replacing them with a single
 * space character. As well as padding the text by `pad` amount with the
 * character `c`.
 *
 * Returns a pointer to the potentially reallocated buffer with no new
 *         new line characters on success. Otherwise returns null on error.
 */
char *prepare_text(char *text, int padd, char c);

#endif
