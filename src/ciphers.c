/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "ciphers.h"

// Transformation functions
char *transform_caesar(caesar_key *key, const char *text, char *dest,
		const int sign);
char *transform_transposition(transposition_key *key, const char *text, char *dest,
		int *f);
char *transform_substitution(substitution_key *key, const char *text, char *dest,
		hash_table *h);
char *transform_vigenere(vigenere_key *key, const char *text, char *dest,
		const int sign);
char *transform_linear(linear_key *key, const char *text, char *dest, const int a,
		int(transform)(int, int, int, int));
char *transform_hill(hill_key *key, const char *text, char *dest, matrix_t *K);

// Utility functions
int encrypt_transform_linear(int a, int i, int b, int n);
int decrypt_transform_linear(int a, int i, int b, int n);
int fill_in_column_matrix(const char *text, int *offset, matrix_t *vector,
		hill_key *key);

/*
 * Create a key that is to be used to encrypt and decrypt text
 * using the transposition cipher. The key struct that is allocated
 * will store the period 'd', and the permutation function 'f' that is
 * specified for the key.
 */
transposition_key *create_transposition_key(int d, int *f)
{
	transposition_key *key = calloc(1, sizeof(struct transposition_key));
	if (!key) {
		fprintf(stderr, "Error: Could not allocate memory for"
				" the transposition key\n");
		return NULL;
	}

	key->d = d;
	key->f = malloc(d*sizeof(int));
	if (!key->f) {
		fprintf(stderr,	"Error cannot allocate memory for f in"
				" transposition key\n");
		free(key);
		key = NULL;
		return NULL;
	}
	memcpy(key->f, f, d*sizeof(int));

	key->f_inv = malloc(d*sizeof(int));
	if (!key->f_inv) {
		fprintf(stderr, "Error cannot allocate memory for f inverse in"
				" transposition key\n");
		free(key->f);
		key->f = NULL;
		free(key);
		key = NULL;
		return NULL;
	}
	int count = 0;
	for (int i = 0; i < d; i++) {
		for (int j = 0; j < d; j++) {
			if (key->f[j] == i+1) {
				key->f_inv[count++] = j+1;
			}
		}
	}

	return key;
}

/*
 * Frees and nullifies the memory allocated for a transposition key.
 */
void delete_transposition_key(transposition_key *key)
{
	free(key->f);
	key->f = NULL;
	free(key->f_inv);
	key->f_inv = NULL;
	free(key);
}

char *transform_transposition(transposition_key *key, const char *text, char *dest,
		int *f)
{
	unsigned long length = (unsigned long)strlen(text);

	if (!(mod(length, key->d) == 0)) {
		fprintf(stderr, "The length of the text is not divisible"
				" by d=%d\n", key->d);
		return NULL;
	}

	for (int i = 0; i<length; i+=key->d) {
		for (int j=0; j<key->d; j++) {
			dest[i+j] = text[i + f[j] - 1];
		}
	}

	dest[length] = '\0';
	return dest;

}

/*
 * Encrypt the text plaintext using the transposition cipher.
 * returns the ciphertext into the dest variable.
 */
char *encrypt_transposition(transposition_key *key, const char *plaintext,
		char *ciphertext)
{
	return transform_transposition(key, plaintext, ciphertext, key->f_inv);
}

/*
 * Decrypt the text ciphertext using the transposition cipher.
 * returns the plaintext into the dest variable.
 */
char *decrypt_transposition(transposition_key *key, const char *ciphertext,
		char *plaintext)
{
	return transform_transposition(key, ciphertext, plaintext, key->f);
}

/*
 * Create an instance of the transposition cipher with the
 * specified period 'd' and permutation function 'f'.
 *
 * Returns: An instance of the transposition cipher with
 *          the specified key. If the creation process fails
 *          frees any allocated memory and returns NULL.
 */
struct Cipher *create_transposition(int d, int *f)
{
	struct Cipher *cipher = malloc(sizeof(struct Cipher));
	if (!cipher) {
		fprintf(stderr, "Error: Could not allocate memory"
				" for transposition Cipher\n");
		return NULL;
	}
	cipher->encrypt = (gen_process)&encrypt_transposition;
	cipher->decrypt = (gen_process)&decrypt_transposition;
	cipher->delete = (key_delete)&delete_transposition_key;

	transposition_key *key = create_transposition_key(d, f);
	if (!key) {
		fprintf(stderr, "Error: Could not a create"
				" transposition key\n");
		free(cipher);
		cipher=NULL;
		return NULL;
	}
	cipher->key = (void*)key;
	cipher->period = key->d;
	cipher->pad_char = 'A';

	return cipher;
}

//-----------------------------------------------------------------------------
/*
 * Create a caesar cipher key to be used for encryption and
 * decryption. The created key stores the alphabet size 'size',
 * and the alphabet shift 'j'.
 *
 * Returns: An Caesar chipher key with the specified parameters.
 *          Otherwise frees any allocated memory and returns NULL.
 */
caesar_key *create_caesar_key(char *alphabet, int j)
{
	caesar_key *key = calloc(1, sizeof(caesar_key));
	if (!key) {
		fprintf(stderr, "Error: Could not allocate memory for"
				" a caesar key\n");
		return NULL;
	}

	key->size = strlen(alphabet);
	key->j = j;
	key->alphabet = strdup(alphabet);

	key->h = hash_create(key->size);
	if (!key->h) {
		fprintf(stderr, "Error: Could not create hash table for a"
				" caesar key\n");
		free(key);
		key = NULL;
		return NULL;
	}

	for (int i = 0; i<key->size; i++) {
		int alpha_return = hash_insert(key->h,
				(char[]){key->alphabet[i], '\0'},
				(void*)(intptr_t)i);
		if (!alpha_return) {
			fprintf(stderr, "Error: Failed to insert value\n");
			free(key);
			return NULL;
		}
	}
	return key;
}

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the caesar key 'key'.
 */
void delete_caesar_key(caesar_key *key)
{
	hash_delete(key->h);
	key->h = NULL;
	free(key->alphabet);
	key->alphabet = NULL;
	free(key);
	//key->h = NULL;
}

char *transform_caesar(caesar_key *key, const char *text, char *dest,
		const int sign)
{
	unsigned long text_size = (unsigned long)strlen(text);
	if (!text_size) {
		return (char*)"";
	}

	for (unsigned long i = 0; i < text_size; i++) {
		int cur_index = (intptr_t)hash_lookup(key->h,
				(char[]){text[i], '\0'});
		if (cur_index == -1) {
			dest[i]	= text[i];
			continue;
		}

		int new_index = mod((cur_index + key->j * sign), key->size);
		dest[i] = key->alphabet[new_index];
	}

	dest[text_size] = '\0';
	return dest;
}

/*
 * Encrypt the plaintext specified in 'plaintext' using the caesar cipher
 * and the key specified in 'key' and return the resulting ciphertext into dest.
 */
char *encrypt_caesar(caesar_key *key, const char *plaintext, char *dest)
{
	return transform_caesar(key, plaintext, dest, 1);
}

/*
 * Decrypt the plaintext specified in 'ciphertext' using the caesar cipher
 * and the key specified in 'key' and return the  resulting plaintext into dest.
 */
char *decrypt_caesar(caesar_key *key, const char *ciphertext, char *dest)
{
	return transform_caesar(key, ciphertext, dest, -1);
}


/*
 * Create an instance of the caesar cipher with the
 * specified alphabet size 'size' and alphabet shift 'j'.
 *
 * Returns: An instance of the caesar cipher with
 *          the specified key. If the creation process fails
 *          frees any allocated memory and returns NULL.
 */
struct Cipher *create_caesar(char *alphabet, int j)
{
	struct Cipher *cipher = malloc(sizeof(struct Cipher));
	if (!cipher) {
		fprintf(stderr, "Error: Could not allocate memory"
				" for caesar Cipher\n");
		return NULL;
	}
	cipher->encrypt = (gen_process)&encrypt_caesar;
	cipher->decrypt = (gen_process)&decrypt_caesar;
	cipher->delete = (key_delete)&delete_caesar_key;

	caesar_key *key = create_caesar_key(alphabet, j);
	if (!key) {
		fprintf(stderr, "Error: Could not a create caesar key\n");
		free(cipher);
		cipher = NULL;
		return NULL;
	}
	cipher->key = (void*)key;
	cipher->period = 1;
	cipher->pad_char = '\0';

	return cipher;
}

//-----------------------------------------------------------------------------
/*
 * Create a key that is to be used to encrypt and decrypt text using the
 * substitution cipher. This key struct stores the alphabet that is to be used
 * for the ciphers operations, the size of the alphabet and hash tables
 * associating the alphabet characters to integers, for instance A=0, B=1,
 * ...,Z=25.
 *
 * returns: A substitution key with the specified parameters.
 *          Otherwise frees any allocated memory and returns NULL.
 */
substitution_key *create_substitution_key(char *alphabet, char *subs)
{
	unsigned long size = (unsigned long)strlen(alphabet);
	substitution_key *key = calloc(1, sizeof(substitution_key));
	if (!key) {
		fprintf(stderr, "Error: Could not allocate memory for a"
				" substitution key\n");
		return NULL;
	}

	//substitution_key *key = calloc(1, sizeof(substitution_key));
	if (!(strlen(alphabet) == strlen(subs))) {
		fprintf(stderr, "Error: length of the alphabet and substitutions"
				" are diffrent.\n");
		return NULL;
	}

	key->alphabet = strdup(alphabet);
	key->size = size;

	key->k = hash_create(size);
	key->k_inverse = hash_create(size);
	if (!key->k || !key->k_inverse) {
		fprintf(stderr,
				"Error: Could not create hash table for a caesar key\n");
		free(key->alphabet);
		key->alphabet = NULL;
		free(key->k);
		key = NULL;
		return NULL;
	}

	for (int i = 0; i<size; i++) {
		int subs_return = hash_insert(key->k,
				(char[]){alphabet[i], '\0'},
				(void*)(intptr_t)subs[i]);
		int alpha_return = hash_insert(key->k_inverse,
				(char[]){subs[i], '\0'},
				(void*)(intptr_t)alphabet[i]);
		if (!subs_return || !alpha_return) {
			fprintf(stderr, "Error: Failed to insert value"
					" in to hash table\n");
			hash_delete(key->k);
			hash_delete(key->k_inverse);
			free(key);
			return NULL;
		}

	}

	return key;
}

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the substitution key 'key'.
 */
void delete_substitution_key(substitution_key *key)
{
	free(key->alphabet);
	key->alphabet = NULL;
	hash_delete(key->k);
	key->k = NULL;
	hash_delete(key->k_inverse);
	key->k_inverse = NULL;
	free(key);

}

char *transform_substitution(substitution_key *key, const char *text,
		char *dest, hash_table *h)
{
	unsigned long text_size = (unsigned long)strlen(text);
	if (!text_size) {
		return (char*)"";
	}

	for (unsigned long i = 0; i < text_size; i++) {
		char sub = (char)(intptr_t)hash_lookup(h, (char[]){text[i], '\0'});
		if (sub == -1) {
			sub = text[i];
		}
		dest[i] = sub;
	}

	dest[text_size] = '\0';
	return dest;
}
/*
 * Encrypt the plaintext specified in 'plaintext' with the key specified
 * in 'key' and return the resulting ciphertext into dest
 */
char *encrypt_substitution(substitution_key *key, const char *plaintext, char *dest)
{
	return transform_substitution(key, plaintext, dest, key->k);
}

/*
 * Decrypt the ciphertext specified in 'ciphertext' with the key specified
 * in 'key' and return the resulting plaintext into dest
 */
char *decrypt_substitution(substitution_key *key, const char *ciphertext, char *dest)
{
	return transform_substitution(key, ciphertext, dest, key->k_inverse);
}

/*
 * Create an instance of the Cipher struct with the substitution cipher
 * functions and key with specified Alphabet and Substitution strings.
 *
 * Returns: An instance of the of the specified substitution cipher.
 *          Otherwise if the creation process fails frees any allocated
 *          memory and returns NULL.
 */
struct Cipher *create_substitution(char *alphabet, char *subs)
{
	struct Cipher *cipher = malloc(sizeof(struct Cipher));
	if (!cipher) {
		fprintf(stderr, "Error: Could not allocate memory"
				" for substitution Cipher\n");
		return NULL;
	}
	cipher->encrypt = (gen_process)&encrypt_substitution;
	cipher->decrypt = (gen_process)&decrypt_substitution;
	cipher->delete = (key_delete)&delete_substitution_key;

	substitution_key *key = create_substitution_key(alphabet, subs);
	if (!key) {
		fprintf(stderr, "Error: Could not create the substitution key\n");
		free(cipher);
		return NULL;
	}
	cipher->key = key;
	cipher->period = 1;
	cipher->pad_char = '\0';

	return cipher;
}
//-----------------------------------------------------------------------------

/*
 * Create a key that is to be used to encrypt and decrypt text using
 * the vigenere cipher. This key struct contains the raw string for the key
 * and the key shifts associated with those characters on each 'alphabet'.
 */
vigenere_key *create_vigenere_key(char *alphabet, char *K)
{
	vigenere_key *key = calloc(1, sizeof(vigenere_key));
	if (!key) {
		fprintf(stderr,
				"Error: Could not allocate memory for the vigenere key\n");
		return NULL;
	}

	key->size = (int)strlen(alphabet);
	key->K_size = (int)strlen(K);

	//Create the hash table and insert all of the alphabet characters
	//and their character positions into the table.
	key->h = hash_create(key->size);
	if (!key->h) {
		fprintf(stderr,
				"Error: Could not create hash table for the vigenere key\n");
		free(key);
		key = NULL;
		return NULL;
	}

	for (int i = 0; i<key->size; i++) {
		int alpha_return = hash_insert(key->h,
				(char[]){alphabet[i], '\0'},
				(void*)(intptr_t)i);
		if (!alpha_return) {
			fprintf(stderr, "Error: Failed to insert value"
					" in to hash table\n");
			hash_delete(key->h);
			free(key);
			return NULL;
		}
	}

	// Allocate the key shifts array and assign the
	// character shifts of the key as its elements.
	key->K_shifts = calloc(key->K_size, sizeof(int));
	if (!key->K_shifts) {
		fprintf(stderr, "Error: cannot allocate memory for the key shifts\n");
		hash_delete(key->h);
		key->h = NULL;
		free(key);
		key = NULL;
		return NULL;
	}

	for (int i = 0; i<key->K_size; i++) {
		key->K_shifts[i] = (int)(intptr_t)hash_lookup(key->h,
				(char[]){K[i], '\0'});
	}

	key->alphabet = strdup(alphabet);
	key->K = strdup(K);
	return key;
}

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the vigenere key 'key'.
 */
void delete_vigenere_key(vigenere_key *key)
{
	free(key->alphabet);
	key->alphabet = NULL;
	free(key->K);
	key->K = NULL;
	hash_delete(key->h);
	key->h = NULL;
	free(key->K_shifts);
	key->K_shifts = NULL;
	free(key);
}

char *transform_vigenere(vigenere_key *key, const char *text, char *dest,
		const int sign)
{
	unsigned long text_size = (unsigned long)strlen(text);
	if (!text_size) {
		return (char*)"";
	}

	if (!(mod(text_size, key->K_size) == 0)) {
		fprintf(stderr, "The length of the text is not divisible"
				" by the key length d = %d\n", key->K_size);
		return NULL;
	}

	for (int i = 0; i < text_size; i+=key->K_size) {
		for (int j = 0; j < key->K_size; j++) {
			int current_index = (intptr_t)hash_lookup(key->h,
					(char[]){text[i+j], '\0'});
			if (current_index == -1) {
				dest[i+j] = text[i+j];
				continue;
			}

			int new_index = mod((current_index + key->K_shifts[j] * sign),
					key->size);
			dest[i+j] = key->alphabet[new_index];
		}
	}

	dest[text_size] = '\0';
	return dest;
}

/*
 * Encrypt the plaintext 'plaintext' using the vigenere cipher, using
 * the key specified in 'key' and return the resulting ciphertext into dest
 */
char *encrypt_vigenere(vigenere_key *key, char *plaintext, char *dest)
{
	return transform_vigenere(key, plaintext, dest, 1);

}

/*
 * Decrypt the ciphertext 'ciphertext' using the vigenere cipher, using
 * the key specified in 'key' and return the resulting plaintext into dest
 */
char *decrypt_vigenere(vigenere_key *key, char *ciphertext, char *dest)
{
	return transform_vigenere(key, ciphertext, dest, -1);
}

/*
 * Create an instance of the Cipher struct with the vigiener functions
 * and key with the specified alphabet and key strings.
 *
 * Returns: An instance of the of the specified vigenere cipher.
 *          Otherwise if the creation process fails frees any allocated
 *          memory and returns NULL.
 */
struct Cipher *create_vigenere(char *alphabet, char *key_str)
{
	struct Cipher *cipher = malloc(sizeof(struct Cipher));
	if (!cipher) {
		fprintf(stderr, "Error: Could not allocate memory"
				" for vigenere Cipher\n");
		return NULL;
	}
	cipher->encrypt = (gen_process)&encrypt_vigenere;
	cipher->decrypt = (gen_process)&decrypt_vigenere;
	cipher->delete = (key_delete)&delete_vigenere_key;

	vigenere_key *key = create_vigenere_key(alphabet, key_str);
	if (!key) {
		fprintf(stderr, "Error: Could not create the vigenere key\n");
		free(cipher);
		cipher = NULL;
		return NULL;
	}
	cipher->key = (void*)key;
	cipher->period = key->K_size;
	cipher->pad_char = key->alphabet[0];

	return cipher;
}

//-----------------------------------------------------------------------------
/*
 * Create a key that is to be used to encrypt and decrypt text using
 * the linear cipher. This key struct contains the raw string for the alphabet
 * that is to be used, as well as the values a and b.
 */
linear_key *create_linear_key(int a, int b, char *alphabet)
{
	linear_key *key = calloc(1, sizeof(linear_key));
	if (!key) {
		fprintf(stderr,
				"Error: Could not allocate memory for a linear key\n");
		return NULL;
	}

	int alphabet_size = (int)strlen(alphabet);
	key->a = a;
	key->b = b;
	key->a_inverse = mod_inverse(a, alphabet_size);
	if (key->a_inverse == -1) {
		fprintf(stderr, "Error: Could not calculate inverse of %d"
				" with alphabet size of %d\n", a, alphabet_size);
		free(key);
		key = NULL;
		return NULL;
	}

	key->h = hash_create(alphabet_size);
	if (!key->h) {
		fprintf(stderr,
				"Error: Could not create hash table for the linear key\n");
		free(key);
		key = NULL;
		return NULL;
	}

	key->size = alphabet_size;
	for (int i = 0; i<alphabet_size; i++) {
		int alpha_return = hash_insert(key->h, (char[]){alphabet[i],'\0'},
				(void*)(intptr_t)i);
		if (!alpha_return) {
			fprintf(stderr, "Error: Failed to insert value"
					" in to hash table\n");
			hash_delete(key->h);
			free(key);
			return NULL;
		}

	}

	key->alphabet = strdup(alphabet);
	return key;
}

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the linear key 'key'
 */
void delete_linear_key(linear_key *key)
{
	free(key->alphabet);
	key->alphabet = NULL;
	hash_delete(key->h);
	key->h = NULL;
	free(key);
}

char *transform_linear(linear_key *key, const char *text, char *dest,
		const int a, int(transform)(int, int, int, int))
{
	unsigned long text_size = (unsigned long)strlen(text);
	if (!text_size) {
		return (char*)"";
	}

	for (unsigned long i = 0; i < text_size; i++) {
		int char_index = (intptr_t)hash_lookup(key->h,
				(char[]){text[i], '\0'});
		if (char_index == -1) {
			dest[i] = text[i];
			continue;
		}

		int new_index = transform(a, char_index, key->b, key->size);
		dest[i] = key->alphabet[new_index];
	}

	dest[text_size] = '\0';
	return dest;
}

int encrypt_transform_linear(int a, int i, int b, int n)
{
	return mod(a * i + b, n);
}

/*
 * Encrypt the plaintext 'plaintext' using the linear cipher, using
 * the key specified in 'key' and return the resulting ciphertext into dest
 */
char *encrypt_linear(linear_key *key, const char *plaintext, char *dest)
{
	return transform_linear(key, plaintext, dest, key->a,
			&encrypt_transform_linear);
}

int decrypt_transform_linear(int a, int i, int b, int n)
{
	return mod(a * (i - b), n);
}

/*
 * Decrypt the ciphertext 'ciphertext' using the linear cipher, using
 * the key specified in 'key' and return the resulting plaintext into dest
 */
char *decrypt_linear(linear_key *key, const char *ciphertext, char *dest)
{
	return transform_linear(key, ciphertext, dest, key->a_inverse,
			&decrypt_transform_linear);
}

/*
 * Create an instance of the linear cipher using the Cipher struct, with
 * the specified alphabet and linear coefficients 'a' and 'b' for the key.
 *
 * Returns: An instance of the of the specified linear cipher.
 *          Otherwise if the creation process fails frees any allocated
 *          memory and returns NULL.
 */
struct Cipher *create_linear(int a, int b, char *alphabet)
{
	struct Cipher *cipher = malloc(sizeof(struct Cipher));
	if (!cipher) {
		fprintf(stderr, "Error: Could not allocate memory"
				" for caesar Cipher\n");
		return NULL;
	}
	cipher->encrypt = (gen_process)&encrypt_linear;
	cipher->decrypt = (gen_process)&decrypt_linear;
	cipher->delete = (key_delete)&delete_linear_key;

	linear_key *key = create_linear_key(a, b, alphabet);
	if (!key) {
		fprintf(stderr, "Error: Could not create the linear key\n");
		free(cipher);
		cipher=NULL;
		return NULL;
	}
	cipher->key = (void*)key;
	cipher->period = 1;
	cipher->pad_char = '\0';

	return cipher;
}

//-----------------------------------------------------------------------------
/*
 * Creates a Hill cipher key that is used to encrypt and decrypt text. The Hill
 * ciphers key that is created is initialised with the key matrix of the size
 * 'd' x 'd', the elements of this matrix are initialised with the vectorized
 * elements stored in 'K_elements'. The created Hill key struct contains both
 * the encryption and decryption matrices (K, and k^-1), as well as the raw
 * defined alphabet string.
 */

hill_key *create_hill_key(char *alphabet, int d, int *K_elements)
{
	hill_key *key = calloc(1, sizeof(hill_key));
	if (!key) {
		fprintf(stderr, "Error: Could not allocate memory for"
				" the hill key\n");
		return NULL;
	}

	key->alphabet = strdup(alphabet);
	key->d = d;
	key->modulus = strlen(alphabet);

	key->h = hash_create(key->modulus);
	if (!key->h) {
		fprintf(stderr,
				"Error: Could not create hash table for the hill key\n");
		free(key);
		return NULL;
	}

	// Insert each of the characters defined in the alphabet and their
	// positions into the hash table.
	for (int i = 0; i<key->modulus; i++) {
		int alpha_return = hash_insert(key->h,
				(char[]){key->alphabet[i], '\0'},
				(void*)(intptr_t)i);
		if (!alpha_return) {
			fprintf(stderr, "Error: Failed to insert value"
					" in to hash table\n");
			hash_delete(key->h);
			free(key);
			return NULL;
		}
	}

	key->K = create_matrix(d, d);
	if (!key->K) {
		fprintf(stderr, "Error: could not create key matrix for"
				" the hill key\n");
		hash_delete(key->h);
		free(key);
		return NULL;
	}

	for (int i = 0; i<d; i++) {
		for (int j = 0; j<d; j++) {
			key->K->data[i][j] = K_elements[i * d + j];
		}
	}

	matrix_mod_inverse(key->K, &key->K_inverse, key->modulus);
	if (!key->K_inverse) {
		fprintf(stderr, "Error: could not create inverse key matrix"
				" for the hill key\n");
		hash_delete(key->h);
		free_matrix(key->K);
		free(key);
		return NULL;

	}
	return key;
}

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the hill key 'key'.
 */
void delete_hill_key(hill_key *key)
{
	free_matrix(key->K);
	key->K = NULL;
	free_matrix(key->K_inverse);
	free(key->alphabet);
	key->alphabet = NULL;
	key->K_inverse = NULL;
	hash_delete(key->h);
	key->h = NULL;
	free(key);
}

int fill_in_column_matrix(const char *text, int *offset, matrix_t *vector,
		hill_key *key)
{
	if (!text || !vector || !key) {
		fprintf(stderr, "Error: Cannot fill in column matrix"
				"an attribute is null\n");
		return 0;
	}

	int vec_index = 0;
	unsigned long text_size = (unsigned long)strlen(text);
	int padd_index = (intptr_t)hash_lookup(key->h, (char[]){key->alphabet[0], '\0'});

	for (int j = *offset; vec_index<key->d && j<text_size; j++) {
		int char_index = (intptr_t)hash_lookup(key->h, (char[]){text[j], '\0'});
		if (char_index == -1) {
			fprintf(stderr, "Warning: unrecognised character skipping\n");
			continue;
		}
		vector->data[vec_index++][0] = char_index;
		*offset = j+1;
	}

	// Pad the column vector if needed with the first character
	// of the defined alphabet in the key
	while (vec_index < key->d) {
		vector->data[vec_index++][0] = padd_index;
		*offset += 1;
	}

	return 1;
}

char *transform_hill(hill_key *key, const char *text, char *dest, matrix_t *K)
{
	unsigned long text_size = (unsigned long)strlen(text);
	int matrix_offset = 0;
	if (text_size == 0) {
		dest[0] = '\0';
		return (char*)"";
	}

	if (!(mod(text_size, key->d) == 0)) {
		fprintf(stderr, "Error: The length of the text is not divisible"
				" by d = %d, Stopping\n", key->d);
		return NULL;
	}

	matrix_t *text_m = create_matrix(key->d, 1);
	matrix_t *transform_m = create_matrix(key->d, 1);
	if (!text_m || !transform_m) {
		fprintf(stderr, "Error: Could not allocate memory for"
				" (cipher/plain)text matrix\n");
		return NULL;
	}

	for (unsigned long i = 0; i<text_size; i+=key->d) {
		int filled = fill_in_column_matrix(text, &matrix_offset, text_m, key);
		if (!filled) {
			fprintf(stderr, "Error: Failed to fill column matrix stopping"
					" transformation\n");
			free_matrix(text_m);
			free_matrix(transform_m);
			return NULL;
		}

		matrix_multi_mod(K, text_m, transform_m, key->modulus);
		for (int n = 0; n<key->d; n++) {
			dest[i+n] = key->alphabet[(int)transform_m->data[n][0]];
			transform_m->data[n][0] = 0;
		}
	}

	free_matrix(text_m);
	free_matrix(transform_m);

	dest[text_size] = '\0';
	return dest;
}

/*
 * Encrypt the plaintext 'plaintext' using the hill cipher, using
 * the key specified in 'key' and return the resulting ciphertext into dest
 */
char *encrypt_hill(hill_key *key, const char *plaintext, char *dest)
{
	return transform_hill(key, plaintext, dest, key->K);
}

/*
 * Decrypt the ciphertext 'ciphertext' using the hill cipher, using
 * the key specified in 'key' and return the resulting plaintext into dest
 */
char *decrypt_hill(hill_key *key, const char *ciphertext, char *dest)
{
	return transform_hill(key, ciphertext, dest, key->K_inverse);
}

/*
 * Create an instance of the Hill cipher using the Cipher struct with the
 * specified 'd'x'd' key matrix and elements.
 *
 * Returns: An allocated instance of the of the specified Hill cipher.
 *          Otherwise if the creation process fails frees any allocated
 *          memory and returns NULL.
 */
struct Cipher *create_hill(char *alphabet, int d, int *K_elements)
{
	struct Cipher *cipher = malloc(sizeof(struct Cipher));
	if (!cipher) {
		fprintf(stderr, "Error: Could not allocate memory"
				" for caesar Cipher\n");
		return NULL;
	}
	cipher->encrypt = (gen_process)&encrypt_hill;
	cipher->decrypt = (gen_process)&decrypt_hill;
	cipher->delete = (key_delete)&delete_hill_key;

	hill_key *key = create_hill_key(alphabet, d, K_elements);
	if (!key) {
		fprintf(stderr, "Error: Could not create the hill key\n");
		free(cipher);
		cipher = NULL;
		return NULL;
	}
	cipher->key = (void*)key;
	cipher->period = d;
	cipher->pad_char = key->alphabet[0];

	return cipher;
}
