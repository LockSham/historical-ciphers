/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include "utils.h"

#define READ_SIZE 512

// Mathematical functions
int mod_power(int base, int exponent, int n);
int fermat_primality(unsigned int n, int k);
int find_prime(int n, int step);

// Hash table functions
unsigned int G(unsigned int S, int c, int d);
unsigned int hash_key(char *key);

/*
 * Read the entire contents of a file into a dynamically allocated buffer.
 *
 * Returns a dynamically allocated char pointer buffer filled with the
 * contents of `file` on success, otherwise returns NULL.
 */
char *read_all(FILE *file)
{
	size_t read_from = 0, size = 0, n = 0;
	char *tmp = NULL, *buffer = NULL;

	if (!file) {
		fprintf(stderr, "Error: cannot read from a null file pointer\n");
		return NULL;
	}

	if (!(buffer = calloc(READ_SIZE+1, sizeof(char)))) {
		fprintf(stderr, "Error: could not allocate memory for buffer\n");
		return NULL;
	}

	while(1) {
		if (READ_SIZE + read_from + 1 > size) {
			// expand the buffer
			size += READ_SIZE + 1;
			// TODO: add overflow check of size
			tmp = realloc(buffer, size);
			if (!tmp) {
				fprintf(stderr, "Error: could not reallocate"
						" memory for the buffer\n");
				free(buffer);
				return NULL;
			}
			buffer = tmp;
		}

		if ((n = fread(buffer + read_from, sizeof(char),
					READ_SIZE, file)) == 0) {
			break;
		}
		read_from += n;
	}

	if (ferror(file) != 0) {
		fprintf(stderr, "Error: Failed to read file\n");
		free(buffer);
		return NULL;
	}

	if (!(tmp = realloc(buffer, read_from + 1))) {
		fprintf(stderr, "Error: Could not reallocate memory to size\n");
		free(buffer);
		return NULL;
	}
	buffer = tmp;

	buffer[read_from] = '\0';
	return buffer;
}

/*
 * Prepare a message to be encrypted/decrypted.
 *
 * Prepares the message `test` for encryption/decryption by removing all
 * the new line characters from the text and replacing them with a single
 * space character. As well as padding the text by `pad` amount with the
 * character `c`.
 *
 * Returns a pointer to the potentially reallocated buffer with no new
 *         new line characters on success. Otherwise returns null on error.
 */
char *prepare_text(char *text, int pad, char c)
{
	int size = strlen(text);
	char *found = text;
	while((found = strchr(found, '\n'))) {
		*found = ' ';
	}
	found = text;

	if (pad > 0) {
		fprintf(stderr, "Note: padding message with %d %c's\n", pad, c);
		size += pad;
		found = realloc(text, size+1);
		if (!found) {
			fprintf(stderr, "Error: could not reallocate"
					" memory for buffer\n");
			return NULL;
		}
		// Pad the text.
		for (int i = size - pad; i < size; i++) {
			found[i] = c;
		}
	}
	found[size] = '\0';
	return found;
}

/*
 * A modulus function that is defined for negative numbers
 * for both a and b. If a is less than 0, C's % operator
 * will return a negative number. To fix this add b to to
 * the remainder. If b < 0 return mod(a,-b), to make b positive.
 */
int mod(int a, int b)
{
	if (b < 0) {
		return mod(a,-b);
	}
	int rem = a % b;
	if (a < 0) {
		return rem+b;
	}

	return a % b;
}

/*
 * Calculate the modular multiplicative inverse of a mod b;
 */
int mod_inverse(int a, int b)
{
	int m = 0, next_m = 1;
	int r = b, next_r = a;
	int quotient, temp;

	while (next_r > 0) {
		quotient = r / next_r;

		temp = m;
		m = next_m;
		next_m = temp - (quotient * next_m);

		temp = r;
		r = next_r;
		next_r = temp - (quotient * next_r);
	}

	if (r > 1) {
		fprintf(stderr, "Error: Modular multiplicative inverse of %d"
				" is not possible\n", a);
		return -1;
	}

	if (m < 0) {
		m += b;
	}
	return m;
}
/*
 * Calculate the GCD(a,b) of a and b using the
 * Euclidean algorithm.
 */
int gcd(int a,int b)
{
	int ai = a;
	int bi = b;
	int remainder = mod(ai,bi);

	while (remainder > 0) {
		remainder = mod(ai,bi);
		ai = bi;
		bi = remainder;
	}

	return ai;

}


/*
 * Strip the new line from the end of the string if it
 * exists
 */
void strip_new_line(char *str)
{
	char *last = str + (strlen(str) - 1);
	if (*last == '\n') {
		*last = '\0';
	}
	return;
}

//------------------------------------------------------------------------------

/*
 * Create and return a new allocated hash_table
 * with the 'size' possible characters of the
 * alphabet. The size of the hash_table is stored
 * in the hash tables size variable.
 *
 * Returns: If the memory is allocated a new hash_table
 *          that has its variables allocated ready for use.
 *          Otherwise returns NULL.
 */
hash_table *hash_create(int size)
{
	int hash_size = next_prime(size);
	int probe_size = prev_prime(size);

	if (gcd(probe_size, hash_size) != 1) {
		fprintf(stderr, "Error: Hash probe size and hash table"
				" size do not have the gcd of 1\n");
		return NULL;
	}

	hash_table *h = calloc(1, sizeof(hash_table));
	if (!h) {
		printf("Error allocating memory for the hash table\n");
		return NULL;
	}

	h->keys = calloc(hash_size, sizeof(char *));
	h->values = calloc(hash_size, sizeof(void *));
	if (!h->keys || !h->values) {
		printf("Error allocating memory for the hash table\n");
		return NULL;
	}
	h->size = hash_size;
	h->probe = probe_size;

	return h;
}

/*
 * Free the allocated memory that is in use by the
 * hash table
 */
void hash_delete(hash_table *h)
{
	for (int i =0; i<h->size; i++) {
		free(h->keys[i]);
	}
	free(h->keys);
	h->keys = NULL;
	free(h->values);
	h->values = NULL;
	free(h);
	h = NULL;
}

int mod_power(int base, int exponent, int n)
{
	int pow = base;
	if (exponent == 0) {
		return 1;
	}
	for (int i = 0; i < exponent-1; i++) {
		pow = mod(pow*base, n);
	}
	return pow;
}

int fermat_primality(unsigned int n, int k)
{
	//base cases
	if (n <= 1 || n == 4) {
		return 0;
	}
	if (n <= 3) {
		return 1;
	}

	for (int i = 0; i < k; i++) {
		int a = 2 + mod(rand(), (n-4));
		if (mod_power(a, n-1, n) != 1) {
			return 0;
		}
	}

	return 1;
}

int find_prime(int n, int step)
{
	int probable_prime = 0;
	while (!probable_prime) {
		n += step;
		probable_prime = fermat_primality(n, 100);
	}

	return n;
}

int next_prime(int n)
{
	return find_prime(n, 1);
}

int prev_prime(int n)
{
	return find_prime(n, -1);
}

unsigned int G(unsigned int S, int c, int d)
{
	return S+c+d*4;
}

/*
 * Hashes the supplied key, to a value that can be modded, to be used as
 * an index or probe length.
 */
unsigned int hash_key(char *key)
{
	int size = strlen(key);
	unsigned int i_key = 137548; // Initial Vector (randomly chosen).
	for (int i = 0; i<size; i++) {
		i_key += G(i_key, key[i], key[i+1]) + i;
	}

	return i_key;
}

/*
 * return the hashed index for the array element that
 * corresponds to the key value.
 */
unsigned int hash_index(int size, char *key)
{
	return mod(hash_key(key), size);
}

unsigned int hash_probe(int size, char *key)
{
	return size - mod(hash_key(key), size);
}

/*
 * Looks up the value associated with the key 'key'.
 *
 * Returns: If a value is associated with the key that value
 *          is returned, otherwise NULL is returned.
 */
void *hash_lookup(hash_table *h, char *key)
{
	unsigned int index = hash_index(h->size, key);
	unsigned int probe = hash_probe(h->probe, key);
	index = mod(index, h->size);

	for (int i = 0; i < h->size && h->keys[index]; i++) {
		if (strcmp(h->keys[index], key) == 0) {
			return h->values[index];
		}
		index = mod(index+probe, h->size);
	}

	return (void*)(intptr_t)-1;
}

/*
 * Insert a new key and value pair into the hash table.
 */
int hash_insert(hash_table *h, char *key, void *value)
{
	unsigned int index = hash_index(h->size, key);
	unsigned int probe = hash_probe(h->probe, key);
	int count = 0;

	//Search for empty an empty space in the hash table
	while (h->keys[index] != NULL) {
		if (count > h->size) {
			fprintf(stderr, "Error: Could not insert new element"
					" into hash table (%s)\n", key);
			return -1;
		}

		// wrap around the values array
		index = mod(index+probe, h->size);
		count++;

	}

	h->keys[index] = strdup(key);
	h->values[index] = value;
	return 1;
}
