/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYS_H_
#define KEYS_H_

/*
 * Defines a data structure to hold a ciphers key data after being read from
 * a key file
 */
struct key_data;

/*
 * Free and nullify the allocated memory used in
 * the key_data structure.
 */
void free_key_data(struct key_data **options);

/*
 * Search the key file for the cipher type. The type declaration
 * is the first non new line, comment line within the key file.
 */
char *read_cipher_type(FILE *key_fp);

/*
 * Get the integer (enum cipher_e) that corresponds to the specified cipher
 * name
 *
 * Returns:
 * 	If cipher_str is a valid cipher name: an integer representing the cipher.
 * Otherwise:
 * 	-1.
 */
int get_cipher_type(char *cipher_str);

/*
 * Process the provided cipher key file for the specified cipher type.
 *
 * Takes the 'cipher_type' to initialise the array of string key attributes
 * for the specified cipher and extracts the attributes specified
 *
 * Returns: extracts the  with the extracted data.
 * else: returns -1 and 'data' is left untouched.
 */
struct key_data *process_key_file(FILE *key_fp, int *cipher_type);

/*
 * Create an instance of the cipher specified by the string cipher_str
 */
struct Cipher *create_cipher(struct key_data *options, int cipher_type);

#endif
