/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "ciphers.h"
#include "keys.h"
#include "matrix.h"
#include "utils.h"
#include "tests.h"

#define BUFFERSIZE (512)

const char *usage_params = "-e|-d [-t] [-o DEST] KEY INFILE\n";
const char *TMP_FILE = "tmp";

char *transform_data(char *buffer, char *text, void *key, gen_process transform);
char *process_data(char *buffer, char *new_data, int size, int encrypt_flag,
		int decrypt_flag, struct Cipher *cipher_s);
struct Cipher *process_cipher(char *key_name);
int read_message(FILE *fp, struct Cipher *cipher, char **file_txt, char **transform_txt);

/*
 * Transform (encrypt/decrypt) the data passed by the buffer with the key
 * and function specified
 *
 * Returns: the transformed (encrypted/decrypted) data.
 */
char *transform_data(char *buffer, char *text, void *key, gen_process transform)
{
	if (*buffer == '\n') {
		text[0] = '\n';
		text[1] = '\0';
		return text;
	}
	char *completed = NULL;
	completed = transform(key, buffer, text);
	if (!completed) {
		fprintf(stderr, "Error: Could not transform buffer\n");
		return NULL;
	}

	//add new null terminator
	int str_len = strlen(text);
	text[str_len] = '\0';
	return text;
}

/*
 * Process the string that is passed by buffer using the specified
 * cipher, and operation.
 */
char *process_data(char *buffer, char *new_data, int size, int encrypt_flag,
		int decrypt_flag, struct Cipher *cipher_s)
{
	// Set the entire string to 0 to prevent leftover characters
	memset(new_data, 0, size);
	char *complete = NULL;
	if (encrypt_flag) {
		complete = transform_data(buffer, new_data, cipher_s->key,
				cipher_s->encrypt);
	} else if (decrypt_flag) {
		complete = transform_data(buffer, new_data, cipher_s->key,
				cipher_s->decrypt);
	}

	if (!complete) {
		fprintf(stderr, "Error: Processing buffer\n");
		return NULL;
	}

	return new_data;
}

/*
 * Process key file 'key_name' and create an instance of the cipher
 * described in the key file.
 *
 * Returns: a pointer to the instance of the cipher described in the key file,
 * otherwise if the cipher has not been successfully created returns NULL.
 */
struct Cipher *process_cipher(char *key_name)
{
	FILE *key_fp = NULL;
	int cipher_type = -1;
	struct key_data *options = NULL;
	struct Cipher *cipher = NULL;

	key_fp = fopen(key_name, "r");
	if (!key_fp) {
		fprintf(stderr, "%s is not a valid file name\n", key_name);
		return NULL;
	}

	options = process_key_file(key_fp, &cipher_type);
	fclose(key_fp);
	if (!options) {
		fprintf(stderr, "Error: Could not read key file correctly\n");
		return NULL;
	}

	cipher = create_cipher(options, cipher_type);
	if (!cipher) {
		fprintf(stderr, "Error: Could not create cipher\n");
		free_key_data(&options);
		return NULL;

	}
	free_key_data(&options);

	return cipher;

}

int read_message(FILE *fp, struct Cipher *cipher, char **file_txt, char **transform_txt)
{
	char *completed = NULL;
	int pad = 0, pad_amount = 0;

	*file_txt = read_all(fp);
	if (!file_txt || !strlen(*file_txt)) {
		fprintf(stderr, "Error: Could not read message text file\n");
		free(*file_txt); // just in case we allocated something
		*file_txt = NULL;
		return -1;
	}

	pad = mod(strlen(*file_txt), cipher->period);
	pad_amount =  pad ? cipher->period - pad : 0;
	completed = prepare_text(*file_txt, pad_amount, cipher->pad_char);
	if (!completed) {
		fprintf(stderr, "Error: Could not prepare message for"
				" transformation\n");
		free(*file_txt);
		*file_txt = NULL;
		return -1;
	}
	*file_txt = completed;

	*transform_txt = calloc(strlen(*file_txt)+1, sizeof(char));
	if (!transform_txt) {
		fprintf(stderr, "Error: Failed to allocate memory for"
				" transformed text\n");
		free(*file_txt);
		*file_txt = NULL;
		return -1;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	FILE *fp, *output_fp;
	char *f_message = NULL, *key_file = NULL, *completed = NULL;
	char *output_name = (char*)TMP_FILE;
	char *new_data = NULL, *file_txt = NULL;
	int opt, encrypt_flag = 0, decrypt_flag = 0, output_flag = 0, test_flag = 0;

	while ((opt = getopt(argc, argv, "edo:t")) != -1) {
		switch (opt) {
		case 'e':
			encrypt_flag = 1;
			break;
		case 'd':
			decrypt_flag = 1;
			break;
		case 'o':
			output_flag = 1;
			output_name = optarg;
			break;
		case 't':
			test_flag = 1;
			break;
		default:
			fprintf(stderr, "Usage: %s %s", argv[0], usage_params);
			return(-1);
		}
	}

	if (test_flag) {
		printf(	"------------------------------------------\n"
			"-----------Running Matrix Tests----------\n"
			"------------------------------------------");
		matrix_test();
		printf(	"\n------------------------------------------\n"
			"-----------Running Cipher Tests-----------\n"
			"------------------------------------------");
		crypt_tests();
		return 0;
	}

	if(!(argc > 3 + (output_flag * 2) + test_flag && (encrypt_flag ^ decrypt_flag))) {
		fprintf(stderr, "Usage: %s %s", argv[0], usage_params);
		return -1;
	}

	f_message = argv[optind+1];
	if (strcmp(f_message, output_name) == 0) {
		output_flag = 0;
		output_name = (char*)TMP_FILE;
	}

	fp = fopen(f_message, "r");
	if (!fp) {
		fprintf(stderr, "Error: %s is not a valid file name\n", f_message);
		return -1;
	}

	output_fp = fopen(output_name, "w");
	if (!output_fp) {
		fprintf(stderr, "Error: could not open %s in write mode\n", output_name);
		fclose(fp);
		return -1;
	}

	key_file = argv[optind];
	struct Cipher *cipher = process_cipher(key_file);
	if (!cipher) {
		fprintf(stderr, "Error: Failed to process cipher\n");
		fclose(fp);
		fclose(output_fp);
		return -1;

	}

	if (read_message(fp, cipher, &file_txt, &new_data) == -1) {
		fprintf(stderr, "Error: Could not read the message, Exiting\n");
		fclose(fp);
		fclose(output_fp);
		cipher->delete(cipher->key);
		free(cipher);
		free(file_txt);
		free(new_data);
		return -1;
	}

	completed = process_data(file_txt, new_data, strlen(file_txt)+1,
			encrypt_flag, decrypt_flag, cipher);
	if (!completed) {
		const char *operation = encrypt_flag ? "encrypt" : "decrypt";
		fprintf(stderr, "Error: Could not %s, %s\n",
				operation, f_message);
		fclose(fp);
		fclose(output_fp);
		cipher->delete(cipher->key);
		free(cipher);
		free(new_data);
		free(file_txt);
		remove(output_name);
		return -1;
	}

	fputs(new_data, output_fp);

	fclose(fp);
	fclose(output_fp);
	cipher->delete(cipher->key);
	free(cipher);
	free(new_data);
	free(file_txt);

	if (!output_flag && rename(TMP_FILE, f_message) == -1) {
		fprintf(stderr, "Error: Could not rename temporary file\n");
		perror("Renaming file");
		return -1;
	}

	return 0;
}
