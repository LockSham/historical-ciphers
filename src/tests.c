/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ciphers.h"
#include "matrix.h"
#include "utils.h"
#include "tests.h"

// Cipher test functions
void transportation_cipher(void);
void caesar_cipher(void);
void substitution_cipher(void);
void vigenere_cipher(void);
void linear_cipher(void);
void hill_cipher(void);

// Utility function
void cipher_test(struct Cipher *cipher, const char *cipher_str, const char *plaintext);

/*
 * Carry out an encryption and decryption test with the specified cipher, on
 * the specified plaintext. The ciphertext that results from the encryption
 * operation is used as the ciphertext input for the decryption test. The out
 * put from the decryption operation (plaintext_prime) is then compared with
 * the original plaintext to determine if the encryption and decryption
 * operations are functionally correct.
 */
void cipher_test(struct Cipher *cipher, const char *cipher_str, const char *plaintext)
{
	unsigned long length_str = (unsigned long)strlen(plaintext);
	char *ciphertext = calloc(length_str+1, sizeof(char));
	char *plaintext_prime = calloc(length_str+1, sizeof(char));

	printf("\n--------%s Cipher Test--------\n", cipher_str);
	char *encrypt = cipher->encrypt(cipher->key, plaintext, ciphertext);
	char *decrypt = cipher->decrypt(cipher->key, ciphertext, plaintext_prime);
	if (!encrypt || !decrypt) {
		cipher->delete(cipher->key);
		cipher->key = NULL;
		free(plaintext_prime);
		free(ciphertext);
		return;
	}
	printf("Original Plaintext: \t'%s'\n", plaintext);
	printf("Ciphertext: \t\t'%s'\n", ciphertext);
	printf("Plaintext Prime: \t'%s'\n", plaintext_prime);

	if (strcmp(plaintext, plaintext_prime) == 0) {
		printf("The decrypted ciphertext is the same as "
				"the orignial plaintext\n");
	} else {
		printf("The decrypted ciphertext is not the same as "
				"the original plaintext\n");
	}

	cipher->delete(cipher->key);
	cipher->key = NULL;
	free(ciphertext);
	free(plaintext_prime);
}

/*
 * Carry out a simple transportation encryption on the
 * currently hardcoded string "CRYPTOGRAPHY".
 * the ciphertext is then returned, then run the ciphertext
 * though the decryption function and check if the plaintext'
 * and plaintext are the same.
 */
void transportation_cipher(void)
{
	int f[] = {2,3,4,1};
	struct Cipher *cipher = create_transposition(4, f);
	const char *plaintext = "CRYPTOGRAPHY";
	cipher_test(cipher, "Transportation", plaintext);
	free(cipher);
	printf("\n");
}

/*
 * Test the Caesar cipher implementation on the string "CIP HER" using the
 * cipher_test() function. The Caesar cipher is tested with a 27 character
 * alphabet and a character shift of 1 (B will be shifted to C etc.).
 */
void caesar_cipher(void)
{
	// hash_table is currently only defined for a 26 character alphabet
	struct Cipher *cipher = create_caesar((char*)"ABCDEFGHIJKLMNOPQRSTUVWXYZ ", 1);
	const char *plaintext = "CIP HER";
	cipher_test(cipher, "Caesar", plaintext);
	free(cipher);
	printf("\n");
}

/*
 * Test the substitution cipher implementation on the string "CIP HER" on a 27
 * character alphabet (all English characters plus space " ") and a substitution
 * string that corresponds to a character shift of 1.
 */
void substitution_cipher(void)
{
	// hash_table is currently only defined for a 26 character alphabet
	struct Cipher *cipher = create_substitution((char*)"ABCDEFGHIJKLMNOPQRSTUVWXYZ ",
		(char*)"BCDEFGHIJKLMNOPQRSTUVWXYZ A");
	const char *plaintext = "CIP HER";
	cipher_test(cipher, "Subsuitution", plaintext);
	//cipher->delete(cipher->key);
	free(cipher);
	printf("\n");

}

/*
 * Test the Vigenere cipher implementation on the string "AT THE TIME " on a 27
 * character alphabet (all English characters plus space " "), using the key
 * "LOCK" which corresponds to a character shift of 11 for the first character,
 * 14 for the second, 2 for the third and 10 for the fourth character.
 */
void vigenere_cipher(void)
{
	// hash_table is currently only defined for a 26 character alphabet
	struct Cipher *cipher = create_vigenere((char*)"ABCDEFGHIJKLMNOPQRSTUVWXYZ ", (char*)"LOCK");
	const char *plaintext = "AT THE TIME ";
	cipher_test(cipher, "Vigenere", plaintext);
	free(cipher);
	printf("\n");
}

/*
 * Test the Linear cipher implementation on the string
 * "IT MUST HAVE BEEN A YOUNG MAN ", on a 27-character alphabet using the
 * linear equation coefficients of a = 2, and b = 5.
 */
void linear_cipher(void)
{
	// hash_table is currently only defined for a 26 character alphabet
	struct Cipher *cipher = create_linear(2, 5, (char*)"ABCDEFGHIJKLMNOPQRSTUVWXYZ ");
	const char *plaintext = "IT MUST HAVE BEEN A YOUNG MAN ";
	cipher_test(cipher, "Linear", plaintext);
	free(cipher);
	printf("\n");
}

/*
 * Test the Hill cipher implementation on the string
 * "!@#$%^&*(*&THIS IS NOT MENT TO HAPPEN ", using a 27 character alphabet
 * with the key matrix as:
 * 	|26	10|
 * 	|15	10|
 * which corresponds to the character matrix of:
 * 	|' '	'K'|
 * 	|'P'	' '|
 */
void hill_cipher(void)
{
	// hash_table is currently only defined for a 26 character alphabet
	int keys[] = {26, 10, 15, 10};
	struct Cipher *cipher = create_hill((char*)"ABCDEFGHIJKLMNOPQRSTUVWXYZ ", 2, keys);
	//char *plaintext = " TO ";
	const char *plaintext = "!@#$%^&*(*&THIS IS NOT MENT TO HAPPEN ";
	cipher_test(cipher, "Hill", plaintext);

	free(cipher);
	printf("\n");
}

/*
 * Run the implementation tests on each of the ciphers, as well as tests
 * on the Modulus and Greatest Common denominator functions.
 */
void crypt_tests(void)
{
	transportation_cipher();
	caesar_cipher();
	substitution_cipher();
	vigenere_cipher();
	linear_cipher();
	hill_cipher();

	printf("\n--------Mod and GCD Tests--------\n");
	int a = -10061, b = 100;
	printf("A=%d, B=%d, a %% b = %d\n", a, b, a%b);
	printf("A=%d, B=%d, a(mod b) = %d\n", a, b, mod(a,b));

	a = 27; b = 16;
	int m = mod_inverse(a, b);
	printf("The modular multiplicative inverse: %d\n", m);

	a = 31; b = 19;
	int r = gcd(a,b);
	printf("The gcd(%d, %d) = %d\n", a, b, r);

//	matrix_test();
}
