/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <error.h>

#include "utils.h"
#include "matrix.h"

void input_values(matrix_t *matrix);

/*
 * Initialise the matrix 'matrix' with values with the values
 * passed to this function. As well as allocating memory
 * for the matrix data.
 */
int init_matrix(matrix_t *matrix, int m, int n)
{
	matrix->m = m;
	matrix->n = n;

	matrix->data = calloc(m, sizeof(double*));
	if (!matrix->data) {
		fprintf(stderr, "Error: cannot allocate memory for"
				" matrix columns\n");
		return -1;
	}

	for (int i = 0; i<m; i++) {
		matrix->data[i] = calloc(n, sizeof(double));
		if (!matrix->data[i]) {
			fprintf(stderr, "Error: Cannot allocate memory for"
					" matrix rows\n");
			for (int j = 0; j<i; j++) {
				free(matrix->data[j]);
				matrix->data[j] = NULL;
			}
			free(matrix->data);
			matrix->data = NULL;
			return -1;
		}

	}

	return 1;
}

/*
 * Create and return a pointer to a newly allocated matrix with
 * its values of m, n being assigned the value passed in m and n respectively.
 * As well as the m*n data array being initialised.
 *
 * returns: matrix if correctly allocated, otherwise returns NULL.
 */
matrix_t *create_matrix(int m, int n)
{
	matrix_t *matrix = calloc(1, sizeof(matrix_t));
	if (!matrix) {
		fprintf(stderr, "Error: Cannot allocate memory for matrix\n");
		return NULL;
	}

	int created_data = init_matrix(matrix, m, n);
	if (!created_data) {
		free(matrix);
		matrix =  NULL;
	}

	return matrix;
}

/*
 * Free and NULL the memory that has been allocated for the matrices
 * data array.
 */
void free_matrix_data(matrix_t *matrix)
{
	if (!matrix || !matrix->data) {
		return;
	}

	for (int i = 0; i<matrix->m; i++) {
		free(matrix->data[i]);
		matrix->data[i] = NULL;
	}

	free(matrix->data);
	matrix->data = NULL;
}

/*
 * Free and NULL the memory that has been allocated for the matrix,
 * data array and the matrix itself.
 */
void free_matrix(matrix_t *matrix)
{
	if (matrix && matrix->data) {
		free_matrix_data(matrix);
	}
	free(matrix);
	matrix = NULL;
	return;
}

/*
 * Transpose the matrix data that is stored in 'matrix' into the matrix pointed
 * to by transpose.
 */
matrix_t *transpose_matrix(matrix_t *matrix, matrix_t *transpose)
{
	if (!transpose) {
		fprintf(stderr, "Error: Cannot allocate memory for matrix");
		return NULL;
	}
	transpose->m = matrix->m;
	transpose->n = matrix->n;

	for (int m = 0; m<matrix->m; m++) {
		for (int n = 0; n<matrix->n; n++) {
			transpose->data[m][n] = matrix->data[n][m];
		}
	}

	return transpose;
}

/*
 * Multiply inplace every element of the matrix by a scala value
 * and return the pointer to the multiplied matrix.
 */
matrix_t *scala_multiplication(matrix_t *matrix, double scala)
{
	for (int m = 0; m < matrix->m; m++) {
		for (int n = 0; n < matrix->n; n++) {
			matrix->data[m][n] *= scala;
		}
	}

	return matrix;
}

/*
 * Multiply inplace every element of the matrix by a scala value
 * modulo the modulus and return the pointer to the multiplied matrix.
 */
matrix_t *scala_multiplication_mod(matrix_t *matrix, double scala, int modulus)
{
	for (int m = 0; m < matrix->m; m++) {
		for (int n = 0; n < matrix->n; n++) {
			matrix->data[m][n] = mod(matrix->data[m][n]*scala, modulus);
		}
	}

	return matrix;
}

/*
 * Perform a matrix multiplication the matrix a and b,
 * and store the result in the matrix multi. Returns the pointer
 * to multi.
 */
matrix_t *matrix_multi(matrix_t *a, matrix_t *b, matrix_t *multi)
{
	for (int a_rows = 0; a_rows < a->m; a_rows++) {
		for (int b_columns = 0; b_columns < b->n; b_columns++) {
			for(int a_columns = 0; a_columns < a->n; a_columns++) {
				multi->data[a_rows][b_columns] +=
					a->data[a_rows][a_columns] *
					b->data[a_columns][b_columns];
			}
		}

	}

	return multi;
}

/*
 * Perform a matrix multiplication on the matrix a and b modulo
 * the modulus, and store the result in the matrix multi. Returns
 * the pointer to the multi matrix.
 */
matrix_t *matrix_multi_mod(matrix_t *a, matrix_t *b, matrix_t *multi, int modulus)
{
	for (int a_rows = 0; a_rows < a->m; a_rows++) {
		for (int b_columns = 0; b_columns < b->n; b_columns++) {
			for(int a_columns = 0; a_columns < a->n; a_columns++) {
				multi->data[a_rows][b_columns] +=
					a->data[a_rows][a_columns] *
					b->data[a_columns][b_columns];
				multi->data[a_rows][b_columns] = mod(multi->data[a_rows][b_columns], modulus);
			}
		}

	}

	return multi;
}

/*
 * Add the elements of the minor matrix that is formed by
 * deleting the row 'm' and column 'n' in 'matrix' and add them to 'cofactor'.
 */
matrix_t *cofactor(matrix_t *matrix, matrix_t *cofactor, int m, int n)
{
	int i = 0, j =0;
	for (int rows = 0; rows<matrix->m; rows++) {
		for (int columns = 0; columns<matrix->n; columns++) {
			if (rows != m && columns != n) {
				cofactor->data[i][j++] = matrix->data[rows][columns];
			}
			if (j == matrix->m-1 ) {
				j = 0;
				i++;
			}
		}
	}

	return cofactor;

}

/*
 * Find and return the determinate of the matrix
 * 'matrix', which has the dimension of 'm'.
 */
double determinate(matrix_t *matrix, int m)
{
	double det = 0;
	int sign = 1;

	// Base Case m == 1: for a single element matrix
	// Base Case m == 2: for a 2x2 matrix
	if (m == 1) {
		return matrix->data[0][0];
	} else if (m == 2) {
		return ((matrix->data[0][0] * matrix->data[1][1]) -
			(matrix->data[0][1] * matrix->data[1][0]));
	}

	matrix_t *minor_m = create_matrix(m, m);
	for (int n = 0; n<m; n++) {
		cofactor(matrix, minor_m, 0, n);
		det += sign * matrix->data[0][n] * determinate(minor_m, m - 1);
		sign  = -sign;
	}

	free_matrix(minor_m);
	return det;
}

/*
 * Find the adjont matrix of 'matrix' add this data to the matrix
 * 'adjoint'. This function first finds the cofactor matrix and then
 * transposes that matrix to give the adjoint matrix.
 */
matrix_t *adjoint(matrix_t *matrix, matrix_t *adjoint)
{
	//TODO add error checking
	double det_minor = 0;
	int sign = 1;
	matrix_t *temp = NULL;
	matrix_t *minor = NULL;

	temp = create_matrix(matrix->m, matrix->n);
	minor = create_matrix(matrix->m-1, matrix->n-1);
	if (!temp || !minor) {
		fprintf(stderr, "Error: Could not allocate memory for"
				" minor/temp matrix\n");
		free_matrix(temp);
		free_matrix(minor);
		return NULL;
	}

	for (int m = 0; m<matrix->m; m++) {
		for (int n = 0; n<matrix->n; n++) {
			cofactor(matrix, minor, m, n);
			det_minor = determinate(minor, matrix->m-1);
			sign = ((m+n)%2==0)? 1:-1;
			temp->data[m][n] = sign * det_minor;
		}
	}

	transpose_matrix(temp, adjoint);

	free_matrix(minor);
	free_matrix(temp);
	return adjoint;
}

/*
 * Calculate the inverse of the matrix 'matrix' into the matrix
 * 'inverse'. This function uses the adjoint method of finding the
 * inverse; Which is efficient for small dimensional square matrices.
 */
matrix_t *matrix_inverse(matrix_t *matrix, matrix_t **inverse)
{
	double det =0;
	matrix_t *adjoint_m = create_matrix(matrix->m, matrix->n);
	adjoint(matrix, adjoint_m);

	det = determinate(matrix, matrix->m);
	if (det == 0) {
		fprintf(stderr, "Error: Cannot, invert the matrix\n");
		return NULL;
	}

	*inverse = scala_multiplication(adjoint_m, (1/det));
	return *inverse;
}

/*
 * Calculate the modular multiplicative inverse of the matrix 'matrix'
 * into the matrix 'inverse'. This function uses the adjoint method
 * of finding the inverse; Which is efficient for small dimensional
 * square matrices.
 */
matrix_t *matrix_mod_inverse(matrix_t *matrix, matrix_t **inverse, int modulus)
{
	double det = 0, mod_det = 0;
	matrix_t *adjoint_m = create_matrix(matrix->m, matrix->n);
	if (!adjoint_m) {
		fprintf(stderr, "Error: Could not allocate memory for"
				" adjoint matrix\n");
		return NULL;
	}
	adjoint(matrix, adjoint_m);

	det = determinate(matrix, matrix->m);
	mod_det = mod_inverse(mod(det, modulus), modulus);
	if ( gcd(mod_det, modulus) != 1) {
		fprintf(stderr, "Error: Modular multiplicative inverse"
				" does not exist\n");
		free_matrix(adjoint_m);
		return NULL;
	}

	*inverse = scala_multiplication_mod(adjoint_m, mod_det, modulus);
	return *inverse;
}

/*
 * Print the matrix 'matrix' out to stdout
 */
void print_matrix(matrix_t *matrix)
{
	for (int y = 0; y<matrix->m; y++) {
		for (int x = 0; x<matrix->n; x++) {
			printf("%.3f\t", matrix->data[y][x]);
		}
		printf("\n");
	}

	return;
}

/*
 * Input test values to the matrix 'matrix'
 */
void input_values(matrix_t *matrix)
{

	matrix->data[0][0] = (double)1;
	matrix->data[0][1] = 4;
	matrix->data[0][2] = 7;

	matrix->data[1][0] = 3;
	matrix->data[1][1] = 0;
	matrix->data[1][2] = 5;

	matrix->data[2][0] = -1;
	matrix->data[2][1] = 9;
	matrix->data[2][2] = 11;

	return;
}

/*
 * Perform various test on each of the functions above
 * This will only print the outcomes and not what should
 * be expected.
 */
int matrix_test(void)
{
	printf("\n----------Print Matrix----------\n");
	matrix_t *matrix = create_matrix(3, 3);
	if (!matrix) {
		printf("Error: did not create the matrix\n");
		return 1;
	}

	input_values(matrix);
	print_matrix(matrix);

	printf("\n----------Print Transpose----------\n");
	matrix_t *transpose = create_matrix(3,3);
	if (!transpose) {
		printf("Error: did not create the matrix\n");
		return 1;
	}
	transpose_matrix(matrix, transpose);
	print_matrix(transpose);

	printf("\n----------Print Scala Multi----------\n");
	scala_multiplication(transpose, 2);
	print_matrix(transpose);

	printf("\n----------Print Matrix Multi----------\n");
	matrix_t *multi = create_matrix(3,3);
	if (!multi) {
		printf("Error: did not create the matrix\n");
		return 1;
	}
	matrix_multi(matrix, matrix, multi);
	print_matrix(multi);

	printf("\n----------Print Minor Matrix----------\n");
	matrix_t *minor_m = create_matrix(matrix->m-1, matrix->n-1);
	if (!minor_m) {
		printf("Error: did not create the matrix\n");
		return 1;
	}
	cofactor(matrix, minor_m, 2-1, 3-1);
	print_matrix(minor_m);

	printf("\n----------Print Determinate----------\n");
	double det = determinate(matrix, 3);
	printf("det(matrix) = %f\n", det);
	det = determinate(minor_m, minor_m->m);
	printf("det(minor) = %.2f\n", det);


	printf("\n----------Print Adjoint----------\n");
	matrix_t *adjoint_m = create_matrix(3,3);
	if (!adjoint_m) {
		printf("Error: did not create the matrix\n");
		return 1;
	}
	adjoint(matrix, adjoint_m);
	print_matrix(adjoint_m);

	printf("\n----------Print Inverse----------\n");
	matrix_t *inverse_m;;
	matrix_inverse(matrix, &inverse_m);
	print_matrix(inverse_m);

	printf("\n----------Print Mod Inverse----------\n");
	matrix_t *inverse_mod;
	matrix_mod_inverse(matrix, &inverse_mod, 27);
	print_matrix(inverse_mod);


	free_matrix(inverse_mod);
	free_matrix(inverse_m);
	free_matrix(adjoint_m);
	free_matrix(minor_m);
	free_matrix(multi);
	free_matrix(transpose);
	free_matrix(matrix);

	return 0;
}
