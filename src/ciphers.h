/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CIPHERS_H_
#define CIPHERS_H_

#include "utils.h"
#include "matrix.h"

/*
 * Defines a generic function pointer that can be used as an
 * easy way to pass a particular ciphers encryption or decryption
 * function
 */
typedef char *(*gen_process)(void *, const char*, char*);
typedef void (*key_delete)(void *);

struct Cipher
{
	void *key;
	int period;
	char pad_char;
	gen_process encrypt;
	gen_process decrypt;
	key_delete delete;
};

enum cipher_e {TRANSPOSITION = 0, CAESAR, SUBSTITUTION, VIGENERE, LINEAR, HILL};

//-----------------------------------------------------------------------------
/*
 * Defines a key data structure for the transposition cipher.
 * The struct contains the following:
 * 	d: The period(length) that is used to permute characters.
 * 	f: An array of permuted character positions of length 'd'.
 */
typedef struct transposition_key {
	int d;
	int *f;
	int *f_inv;
} transposition_key;

/*
 * Create a key that is to be used to encrypt and decrypt text
 * using the transposition cipher.
 */
transposition_key *create_transposition_key(int d, int *f);

/*
 * Frees and nullifies the memory allocated for a transposition key.
 */
void delete_transposition_key(transposition_key *key);

/*
 * Encrypt the text plaintext using the transposition cipher.
 * returns the ciphertext into the dest variable.
 */
char *encrypt_transposition(transposition_key *key, const char *plaintext, char *dest);

/*
 * Decrypt the text ciphertext using the transposition cipher.
 * returns the plaintext into the dest variable.
 */
char *decrypt_transposition(transposition_key *key, const char *ciphertext, char *dest);

/*
 * Create an instance of the transposition cipher with the
 * specified period 'd' and permutation function 'f'.
 *
 * Returns: An instance of the transposition cipher with
 *          the specified key. If the creation process fails
 *          frees any allocated memory and returns NULL.
 */
struct Cipher *create_transposition(int d, int *f);

//-----------------------------------------------------------------------------

/*
 * Defines a key data structure for the Caesar cipher.
 * The key contains the following:
 * 	size: The size of defined alphabet.
 * 	j: Integer representing the number of characters shifted
 * 	   by the key.
 * 	alphabet: The alphabet that is defined by the by the cipher.
 * 	h: Hash table filled with the defined alphabets characters as keys
 * 	   and character position in the alphabet as values.
 */
typedef struct caesar_key {
	int size;
	int j;
	char *alphabet;
	hash_table *h;
} caesar_key;

/*
 * Create a caesar cipher key to be used for encryption and
 * decryption. The created key stores the alphabet size 'size',
 * and the alphabet shift 'j'.
 *
 * Returns: An Caesar chipher key with the specified parameters.
 *          Otherwise frees any allocated memory and returns NULL.
 */
caesar_key *create_caesar_key(char *alphabet, int j);

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the caesar key 'key'.
 */
void delete_caesar_key(caesar_key *key);

/*
 * Encrypt the plaintext specified in 'plaintext' using the caesar cipher
 * and the key specified in 'key' and return the resulting ciphertext into dest.
 */
char *encrypt_caesar(caesar_key *key, const char *plaintext, char *dest);

/*
 * Decrypt the plaintext specified in 'ciphertext' using the caesar cipher
 * and the key specified in 'key' and return the  resulting plaintext into dest.
 */
char *decrypt_caesar(caesar_key *key, const char *ciphertext, char *dest);

/*
 * Create an instance of the caesar cipher with the
 * specified alphabet size 'size' and alphabet shift 'j'.
 *
 * Returns: An instance of the caesar cipher with
 *          the specified key. If the creation process fails
 *          frees any allocated memory and returns NULL.
 */
struct Cipher *create_caesar(char *alphabet, int j);


//-----------------------------------------------------------------------------

/*
 * Defines a key data structure for the Substitution cipher.
 * The key contains the following:
 * 	size: The size of the defined alphabet
 * 	alphabet: The raw string representation of the alphabet.
 * 	k: A hash table representing the alphabets character as keys and
 *	   each character’s position in the alphabet as values.
 *	k_inverse: A hash table representing the substitution alphabets
 *		   characters as keys and each character’s position as values.
 */
typedef struct substitution_key {
	int size;
	char *alphabet;
	hash_table *k;
	hash_table *k_inverse;
} substitution_key;

/*
 * Create a key that is to be used to encrypt and decrypt text using the
 * substitution cipher. This key struct stores the alphabet that is to be used
 * for the ciphers operations, the size of the alphabet and hash tables
 * associating the alphabet characters to integers, for instance A=0, B=1,
 * ...,Z=25.
 *
 * returns: A substitution key with the specified parameters.
 *          Otherwise frees any allocated memory and returns NULL.
 */
substitution_key *create_substitution_key(char *alphabet, char *subs);

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the substitution key 'key'.
 */
void delete_substitution_key(substitution_key *key);

/*
 * Encrypt the plaintext specified in 'plaintext' with the key specified
 * in 'key' and return the resulting ciphertext into dest
 */
char *encrypt_substitution(substitution_key *key, const char *plaintext, char *dest);

/*
 * Decrypt the ciphertext specified in 'ciphertext' with the key specified
 * in 'key' and return the resulting plaintext into dest
 */
char *decrypt_substitution(substitution_key *key, const char *ciphertext, char *dest);

/*
 * Create an instance of the Cipher struct with the substitution cipher
 * functions and key with specified Alphabet and Substitution strings.
 *
 * Returns: An instance of the of the specified substitution cipher.
 *          Otherwise if the creation process fails frees any allocated
 *          memory and returns NULL.
 */
struct Cipher *create_substitution(char *alphabet, char *subs);

//-----------------------------------------------------------------------------

/*
 * Defines a key data structure for the Vigenere cipher.
 * The key structure contains the following:
 * 	size: The size of the defined alphabet.
 * 	K_size: The size of the key string.
 * 	alphabet: The raw string of alphabet characters.
 * 	K: The key string (each character in this string represents a character
 * 	   shift of the plaintext/ciphertext).
 * 	K_shifts: An array of the character shift from the key string 'K'.
 * 	h: A hash table representing the defined character and their positions
 * 	   as key/value pairs.
 */
typedef struct vigenere_key {
	int size;
	int K_size;
	char *alphabet;
	char *K;
	int *K_shifts;
	hash_table *h;
} vigenere_key;

/*
 * Create a key that is to be used to encrypt and decrypt text using
 * the vigenere cipher. This key struct contains the raw string for the key
 * and the key shifts associated with those characters on each 'alphabet'.
 */
vigenere_key *create_vigenere_key(char *alphabet, char *key);

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the vigenere key 'key'.
 */
void delete_vigenere_key(vigenere_key *key);

/*
 * Encrypt the plaintext 'plaintext' using the vigenere cipher, using
 * the key specified in 'key' and return the resulting ciphertext into dest
 */
char *encrypt_vigenere(vigenere_key *key, char *plaintext, char *dest);

/*
 * Decrypt the ciphertext 'ciphertext' using the vigenere cipher, using
 * the key specified in 'key' and return the resulting plaintext into dest
 */
char *decrypt_vigenere(vigenere_key *key, char *ciphertext, char *dest);

/*
 * Create an instance of the Cipher struct with the vigiener functions
 * and key with the specified alphabet and key strings.
 *
 * Returns: An instance of the of the specified vigenere cipher.
 *          Otherwise if the creation process fails frees any allocated
 *          memory and returns NULL.
 */
struct Cipher *create_vigenere(char *alphabet, char *key_str);

//-----------------------------------------------------------------------------
/*
 * Defines a key data structure for the Linear cipher.
 * The key structure contains the following:
 *	a: The coefficient 'a' of the linear equation.
 *	a_inverse: The modular multiplicative inverse of the coefficient a.
 *	b: The coefficient 'b' of the linear equation.
 *	alphabet: The raw string representation of the defined alphabet.
 *	h: A hash table representing the defined characters and their positions
 *	   as key/value pairs.
 */
typedef struct linear_key {
	int size;
	int a;
	int a_inverse;
	int b;
	char *alphabet;
	hash_table *h;
} linear_key;

/*
 * Create a key that is to be used to encrypt and decrypt text using
 * the linear cipher. This key struct contains the raw string for the alphabet
 * that is to be used, as well as the values a and b.
 */
linear_key *create_linear_key(int a, int b, char *alphabet);

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the linear key 'key'
 */
void delete_linear_key(linear_key *key);

/*
 * Encrypt the plaintext 'plaintext' using the linear cipher, using
 * the key specified in 'key' and return the resulting ciphertext into dest
 */
char *encrypt_linear(linear_key *key, const char *plaintext, char *dest);

/*
 * Decrypt the ciphertext 'ciphertext' using the linear cipher, using
 * the key specified in 'key' and return the resulting plaintext into dest
 */
char *decrypt_linear(linear_key *key, const char *ciphertext, char *dest);

/*
 * Create an instance of the linear cipher using the Cipher struct, with
 * the specified alphabet and linear coefficients 'a' and 'b' for the key.
 *
 * Returns: An instance of the of the specified linear cipher.
 *          Otherwise if the creation process fails frees any allocated
 *          memory and returns NULL.
 */
struct Cipher *create_linear(int a, int b, char *alphabet);

//-----------------------------------------------------------------------------

/*
 * Defines a key data structure for the Hill cipher.
 * The key contains the following:
 * 	alphabet: The raw string representation of all the defined alphabet
 * 	          characters.
 * 	modulus: The modulus to use in all equations.
 * 	d: The length of the key matrices rows, and columns.
 * 	K: Key matrix that is used for encryption.
 * 	K_inverse: Inverse of the Key matrix to be used for decryption.
 * 	h: Hash table representing the defined characters and their positions
 * 	   as key/value pairs.
 */
typedef struct hill_key {
	char *alphabet;
	int modulus;
	int d;
	matrix_t *K;
	matrix_t *K_inverse;
	hash_table *h;
} hill_key;

/*
 * Creates a Hill cipher key that is used to encrypt and decrypt text. The Hill
 * ciphers key that is created is initialised with the key matrix of the size
 * 'd' x 'd', the elements of this matrix are initialised with the vectorized
 * elements stored in 'K_elements'. The created Hill key struct contains both
 * the encryption and decryption matrices (K, and k^-1), as well as the raw
 * defined alphabet string.
 */
hill_key *create_hill_key(char *alphabet, int d, int *K_elements);

/*
 * Frees and nullifies the memory allocated on the heap, for
 * the instance of the hill key 'key'.
 */
void delete_hill_key(hill_key *key);

/*
 * Encrypt the plaintext 'plaintext' using the hill cipher, using
 * the key specified in 'key' and return the resulting ciphertext into dest
 */
char *encrypt_hill(hill_key *key, const char *plaintext, char *dest);

/*
 * Decrypt the ciphertext 'ciphertext' using the hill cipher, using
 * the key specified in 'key' and return the resulting plaintext into dest
 */
char *decrypt_hill(hill_key *key, const char *ciphertext, char *dest);

/*
 * Create an instance of the Hill cipher using the Cipher struct with the
 * specified 'd'x'd' key matrix and elements.
 *
 * Returns: An allocated instance of the of the specified Hill cipher.
 *          Otherwise if the creation process fails frees any allocated
 *          memory and returns NULL.
 */
struct Cipher *create_hill(char *alphabet, int d, int *K_elements);

#endif
