/*
 * Historical Ciphers - Implementation of six historical ciphers
 * Copyright (C) 2020  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MATRIX_H_
#define MATRIX_H_
/*
 * Defines a matrix data structure that contains the elements of a matrix, as
 * well as the number of rows 'm' and number of columns 'n' of the matrix.
 */
typedef struct matrix_t {
	double **data;
	int m;
	int n;
} matrix_t;

/*
 * Initialise the matrix 'matrix' with values with the values
 * passed to this function. As well as allocating memory
 * for the matrix data.
 */
int init_matrix(matrix_t *matrix, int m, int n);

/*
 * Create and return a pointer to a newly allocated matrix with
 * its values of m, n being assigned the value passed in m and n respectivly.
 * As well as the m*n data array being initialised.
 *
 * @returns matrix if correctly allocated, otherwise returns NULL.
 */
matrix_t *create_matrix(int m, int n);

/*
 * Free and NULL the memory that has been allocated for the matrixs
 * data array.
 */
void free_matrix_data(matrix_t *matrix);

/*
 * Free and NULL the memory that has been allocated for the matrix,
 * data array and the matrix itself.
 */
void free_matrix(matrix_t *matrix);

/*
 * Transpose the matrix data that is stored in 'matrix' into the matrix pointed
 * to by transpose.
 */
matrix_t *transpose_matrix(matrix_t *matrix, matrix_t *transpose);

/*
 * Multiply inplace every element of the matrix by a scala value
 * and return the pointer to the multiplied matrix.
 */
matrix_t *scala_multiplication(matrix_t *matrix, double scala);

/*
 * Multiply inplace every element of the matrix by a scala value
 * modulo the modulus and return the pointer to the multiplied matrix.
 */
matrix_t *scala_multiplication_mod(matrix_t *matrix, double scala, int modulus);

/*
 * Perform a matrix multiplication the the matrix a and b,
 * and store the result in the matrix multi. Returns the pointer
 * to multi.
 */
matrix_t *matrix_multi(matrix_t *a, matrix_t *b, matrix_t *multi);

/*
 * Perform a matrix multiplication on the matrix a and b modulo
 * the modulus, and store the result in the matrix multi. Returns
 * the pointer to the multi matrix.
 */
matrix_t *matrix_multi_mod(matrix_t *a, matrix_t *b, matrix_t *multi, int modulus);

/*
 * Add the elements of the minor matrix that is formed by
 * deleting the row 'm' and column 'n' in 'matrix' and add them to 'cofactor'.
 */
matrix_t *cofactor(matrix_t *matrix, matrix_t *cofactor, int m, int n);

/*
 * Find and return the determinate of the matrix
 * 'matrix', which has the dimension of 'm'.
 */
double determinate(matrix_t *matrix, int m);

/*
 * Find the adjont matrix of 'matrix' add this data to the matrix
 * 'adjoint'. This function first finds the cofactor matrix and then
 * transposes that matrix to give the adjoint matrix.
 */
matrix_t *adjoint(matrix_t *matrix, matrix_t *adjoint);

/*
 * Calculate the inverse of the matrix 'matrix' into the matrix
 * 'inverse'. This function uses the adjoint method of finding the
 * inverse; Which is efficient for small dimensional square matrices.
 */
matrix_t *matrix_inverse(matrix_t *matrix, matrix_t **inverse);

/*
 * Calculate the modular multiplicative inverse of the matrix 'matrix'
 * into the matrix 'inverse'. This function uses the adjoint method
 * of finding the inverse; Which is efficient for small dimensional
 * square matrices.
 */
matrix_t *matrix_mod_inverse(matrix_t *matrix, matrix_t **inverse, int modulus);

/*
 * Print the matrix 'matrix' out to stdout
 */
void print_matrix(matrix_t *matrix);

/*
 * Perform various test on each of the functions above
 * This will only print the outcomes and not what should
 * be expected.
 */
int matrix_test(void);

#endif
